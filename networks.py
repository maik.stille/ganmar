import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils import spectral_norm as spectral_norm_fn
from torch.nn.utils import weight_norm as weight_norm_fn
from PIL import Image
from torchvision import transforms
from torchvision import utils as vutils


class Conv2D(nn.Module):
    def __init__(self, input_dim, output_dim, kernel_size, stride, padding=0,
                 conv_padding=0, dilation=1, weight_norm='none', norm='none',
                 activation='relu', pad_type='zero', transpose=False):
        super(Conv2D, self).__init__()
        self.use_bias = True
        # initialize padding
        if pad_type == 'reflect':
            self.pad = nn.ReflectionPad2d(padding)
        elif pad_type == 'zero':
            self.pad = nn.ZeroPad2d(padding)
        elif pad_type == 'replicate':
            self.pad = nn.ReplicationPad2d(padding)
        elif pad_type == 'none':
            self.pad = None
        else:
            assert 0, "Unsupported padding: {}".format(pad_type)

        # initialize normalization
        norm_dim = output_dim
        if norm == 'bn':
            self.norm = nn.BatchNorm2d(norm_dim)
        elif norm == 'in':
            self.norm = nn.InstanceNorm2d(norm_dim)
        elif norm == 'none':
            self.norm = None
        else:
            assert 0, "Unsupported normalization: {}".format(norm)

        if weight_norm == 'sn':
            self.weight_norm = spectral_norm_fn
        elif weight_norm == 'wn':
            self.weight_norm = weight_norm_fn
        elif weight_norm == 'none':
            self.weight_norm = None
        else:
            assert 0, "Unsupported normalization: {}".format(weight_norm)

        # initialize activation
        if activation == 'relu':
            self.activation = nn.ReLU(inplace=True)
        elif activation == 'elu':
            self.activation = nn.ELU(inplace=True)
        elif activation == 'lrelu':
            self.activation = nn.LeakyReLU(0.2, inplace=True)
        elif activation == 'prelu':
            self.activation = nn.PReLU()
        elif activation == 'selu':
            self.activation = nn.SELU(inplace=True)
        elif activation == 'tanh':
            self.activation = nn.Tanh()
        elif activation == 'none':
            self.activation = None
        else:
            assert 0, "Unsupported activation: {}".format(activation)

        # define convolution type
        if transpose:
            self.conv = nn.ConvTranspose2d(input_dim, output_dim,
                                           kernel_size, stride,
                                           padding=conv_padding,
                                           output_padding=conv_padding,
                                           dilation=dilation,
                                           bias=self.use_bias)
        else:
            self.conv = nn.Conv2d(input_dim, output_dim, kernel_size, stride,
                                  padding=conv_padding, dilation=dilation,
                                  bias=self.use_bias)

        if self.weight_norm:
            self.conv = self.weight_norm(self.conv)

    def forward(self, x):
        if self.pad:
            x = self.conv(self.pad(x))
        else:
            x = self.conv(x)
        if self.norm:
            x = self.norm(x)
        if self.activation:
            x = self.activation(x)
        return x


class Generator(nn.Module):
    def __init__(self, config, use_cuda, device_ids):
        super(Generator, self).__init__()
        self.input_dim = config['input_dim']
        self.cnum = config['ngf']
        self.use_cuda = use_cuda
        self.device_ids = device_ids

        self.coarse_generator = CoarseGen(self.input_dim, self.cnum, self.use_cuda, self.device_ids)
        self.fine_generator = FineGen(self.input_dim, self.cnum, self.use_cuda, self.device_ids)

    def forward(self, x, mask):
        x_coarse = self.coarse_generator(x, mask)
        x_fine = self.fine_generator(x, x_coarse, mask)
        return x_coarse, x_fine


class CoarseGen(nn.Module):
    def __init__(self, input_dim, cnum, use_cuda=True, device_ids=None):
        super(CoarseGen, self).__init__()
        self.use_cuda = use_cuda
        self.device_ids = device_ids

        # 2 x 128 x 128
        self.conv1 = generator_conv(input_dim + 1, cnum, 5, 1, 2)
        self.conv2_dsample = generator_conv(cnum, cnum*2, 3, 2, 1)

        # cnum*2 x 64 x 64
        self.conv3 = generator_conv(cnum*2, cnum*2, 3, 1, 1)
        self.conv4_dsample = generator_conv(cnum*2, cnum*4, 3, 2, 1)

        # cnum*4 x 32 x 32
        self.conv5 = generator_conv(cnum*4, cnum*4, 3, 1, 1)
        self.conv6 = generator_conv(cnum*4, cnum*4, 3, 1, 1)

        self.conv7 = generator_conv(cnum*4, cnum*4, 3, 1, 2, dil=2)
        self.conv8 = generator_conv(cnum*4, cnum*4, 3, 1, 4, dil=4)
        self.conv9 = generator_conv(cnum*4, cnum*4, 3, 1, 8, dil=8)
        self.conv10 = generator_conv(cnum*4, cnum*4, 3, 1, 16, dil=16)

        self.conv11 = generator_conv(cnum*4, cnum*4, 3, 1, 1)
        self.conv12 = generator_conv(cnum*4, cnum*4, 3, 1, 1)

        # cnum*4 x 64 x 64
        self.conv13 = generator_conv(cnum*4, cnum*2, 3, 1, 1)
        self.conv14 = generator_conv(cnum*2, cnum*2, 3, 1, 1)
        self.conv15 = generator_conv(cnum*2, cnum, 3, 1, 1)
        self.conv16 = generator_conv(cnum, cnum//2, 3, 1, 1)
        self.conv17 = generator_conv(cnum//2, input_dim, 3, 1, 1, activation='none')
        # 1 x 128 x 128

    def forward(self, x, mask):

        if self.use_cuda:
            mask = mask.cuda()

        x = self.conv1(torch.cat([x, mask], dim=1))
        xb_1 = self.conv2_dsample(x)

        xb_1 = self.conv3(xb_1)
        xb_2 = self.conv4_dsample(xb_1)

        x = self.conv5(xb_2)
        x = self.conv6(x)
        x = self.conv7(x)
        x = self.conv8(x)
        x = self.conv9(x)
        x = self.conv10(x)
        x = self.conv11(x)
        x = self.conv12(x) + xb_2
        x = F.interpolate(x, scale_factor=2, mode='nearest')

        x = self.conv13(x) + xb_1
        x = self.conv14(x)
        x = F.interpolate(x, scale_factor=2, mode='nearest')

        x = self.conv15(x)
        x = self.conv16(x)
        x = self.conv17(x)

        x_stage1 = torch.clamp(x, -1., 1.)

        return x_stage1


class FineGen(nn.Module):
    def __init__(self, input_dim, cnum, use_cuda=True, device_ids=None):
        super(FineGen, self).__init__()
        self.use_cuda = use_cuda
        self.device_ids = device_ids

        # 2 x 128 x 128
        self.conv1 = generator_conv(input_dim + 1, cnum, 5, 1, 2)
        self.conv2_downsample = generator_conv(cnum, cnum, 3, 2, 1)
        # cnum*2 x 64 x 64
        self.conv3 = generator_conv(cnum, cnum*2, 3, 1, 1)
        self.conv4_downsample = generator_conv(cnum*2, cnum*2, 3, 2, 1)
        # cnum*4 x 32 x 32
        self.conv5 = generator_conv(cnum*2, cnum*4, 3, 1, 1)
        self.conv6 = generator_conv(cnum*4, cnum*4, 3, 1, 1)

        self.conv7 = generator_conv(cnum*4, cnum*4, 3, 1, 2, dil=2)
        self.conv8 = generator_conv(cnum*4, cnum*4, 3, 1, 4, dil=4)
        self.conv9 = generator_conv(cnum*4, cnum*4, 3, 1, 8, dil=8)
        self.conv10 = generator_conv(cnum*4, cnum*4, 3, 1, 16, dil=16)

        self.conv11 = generator_conv(cnum*4, cnum*4, 3, 1, 1)
        self.conv12 = generator_conv(cnum*4, cnum*4, 3, 1, 1)
        self.conv13 = generator_conv(cnum*4, cnum*2, 3, 1, 1)
        self.conv14 = generator_conv(cnum*2, cnum*2, 3, 1, 1)
        self.conv15 = generator_conv(cnum*2, cnum, 3, 1, 1)
        self.conv16 = generator_conv(cnum, cnum//2, 3, 1, 1)
        self.conv17 = generator_conv(cnum//2, input_dim, 3, 1, 1, activation='none')

    def forward(self, xin, x_stage1, mask):

        x1_inpaint = x_stage1 * mask + xin * (1. - mask)

        if self.use_cuda:
            mask = mask.cuda()

        x_in_mask = torch.cat([x1_inpaint, mask], dim=1)
        x = self.conv1(x_in_mask)
        x = self.conv2_downsample(x)
        x = self.conv3(x)
        x = self.conv4_downsample(x)
        x = self.conv5(x)
        x = self.conv6(x)
        x = self.conv7(x)
        x = self.conv8(x)
        x = self.conv9(x)
        x = self.conv10(x)

        x = self.conv11(x)
        x = self.conv12(x)
        x = F.interpolate(x, scale_factor=2, mode='nearest')
        x = self.conv13(x)
        x = self.conv14(x)
        x = F.interpolate(x, scale_factor=2, mode='nearest')
        x = self.conv15(x)
        x = self.conv16(x)
        x = self.conv17(x)
        x_stage2 = torch.clamp(x, -1., 1.)

        return x_stage2


def generator_conv(input_dim, output_dim, kernel_size=3, stride=1, padding=0, dil=1,
             activation='elu'):
    return Conv2D(input_dim, output_dim, kernel_size, stride,
                       conv_padding=padding, dilation=dil,
                       activation=activation)


class discriminator(nn.Module):
    def __init__(self, config, use_cuda=True, device_ids=None):
        super(discriminator, self).__init__()
        self.input_dim = config['input_dim']
        self.use_cuda = use_cuda
        self.device_ids = device_ids
    # discriminator model

        self.t1 = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=32, kernel_size=(4, 4), stride=2, padding=1),
            nn.LeakyReLU(0.2, inplace=True)
        )
        self.t2 = nn.Sequential(
            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=(4, 4), stride=2, padding=1),
            nn.LeakyReLU(0.2, inplace=True)
        )

        self.t3 = nn.Sequential(
            nn.Conv2d(in_channels=64, out_channels=128, kernel_size=(4, 4), stride=2, padding=1),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.2, inplace=True)
        )

        self.t4 = nn.Sequential(
            nn.Conv2d(in_channels=128, out_channels=256, kernel_size=(4, 4), stride=2, padding=1),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.2, inplace=True)
        )
        self.t5 = nn.Sequential(
            nn.Conv2d(in_channels=256, out_channels=512, kernel_size=(4, 4), stride=2, padding=1),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(0.2, inplace=True)
        )
        self.t6 = nn.Sequential(
            nn.Conv2d(in_channels=512, out_channels=1, kernel_size=(4, 4), stride=2, padding=0),
            nn.Sigmoid()
        )

    def forward(self, x):
        x = self.t1(x)
        x = self.t2(x)
        x = self.t3(x)
        x = self.t4(x)
        x = self.t5(x)
        x = self.t6(x)
        return x  # output of discriminator


class discriminator_256(nn.Module):
    def __init__(self, config, use_cuda=True, device_ids=None):
        super(discriminator_256, self).__init__()
        self.input_dim = config['input_dim']
        self.use_cuda = use_cuda
        self.device_ids = device_ids
    # discriminator model

        self.t1 = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=16, kernel_size=(4, 4), stride=2, padding=1),
            nn.LeakyReLU(0.2, inplace=True)
        )
        self.t2 = nn.Sequential(
            nn.Conv2d(in_channels=16, out_channels=32, kernel_size=(4, 4), stride=2, padding=1),
            nn.LeakyReLU(0.2, inplace=True)
        )
        self.t3 = nn.Sequential(
            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=(4, 4), stride=2, padding=1),
            nn.LeakyReLU(0.2, inplace=True)
        )

        self.t4 = nn.Sequential(
            nn.Conv2d(in_channels=64, out_channels=128, kernel_size=(4, 4), stride=2, padding=1),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.2, inplace=True)
        )

        self.t5 = nn.Sequential(
            nn.Conv2d(in_channels=128, out_channels=256, kernel_size=(4, 4), stride=2, padding=1),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.2, inplace=True)
        )
        self.t6 = nn.Sequential(
            nn.Conv2d(in_channels=256, out_channels=512, kernel_size=(4, 4), stride=2, padding=1),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(0.2, inplace=True)
        )
        self.t7 = nn.Sequential(
            nn.Conv2d(in_channels=512, out_channels=1, kernel_size=(4, 4), stride=2, padding=0),
            nn.Sigmoid()
        )

    def forward(self, x):
        x = self.t1(x)
        x = self.t2(x)
        x = self.t3(x)
        x = self.t4(x)
        x = self.t5(x)
        x = self.t6(x)
        x = self.t7(x)
        return x
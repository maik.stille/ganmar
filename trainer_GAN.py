import os
import torch
import torch.nn as nn
from torch import autograd
from networks import Generator, discriminator, discriminator_256
from torch.autograd import Variable
from help_functions import get_model_list, local_patch, get_logger

logger = get_logger()


class Trainer_GAN(nn.Module):
    def __init__(self, config):
        super(Trainer_GAN, self).__init__()
        self.config = config
        self.use_cuda = self.config['cuda']
        self.device_ids = self.config['gpu_ids']

        # define Networks
        self.netG = Generator(self.config['netG'], self.use_cuda, self.device_ids)
        if self.config['patch_size'] == 128:
            self.netD = discriminator(self.config['netG'], self.use_cuda, self.device_ids)
        else:
            self.netD = discriminator_256(self.config['netG'], self.use_cuda, self.device_ids)

        # define optimizer
        self.optimizer_g = torch.optim.Adam(self.netG.parameters(), lr=self.config['lr'],
                                            betas=(self.config['beta1'], self.config['beta2']))
        d_params = list(self.netD.parameters())
        self.optimizer_d = torch.optim.Adam(d_params, lr=config['lr'],
                                            betas=(self.config['beta1'], self.config['beta2']))
        if self.use_cuda:
            self.netG.to(self.device_ids[0])
            self.netD.to(self.device_ids[0])

        # prepare real and fake labels for the Discriminator
        self.label = torch.FloatTensor(self.config['batch_size']).cuda()
        self.real_label = 1
        self.fake_label = 0
        self.label = Variable(self.label)

    def forward(self, x, box, mask, ground_truth, fp):
        self.train()
        l1_loss = nn.L1Loss()
        bce_loss = nn.BCELoss()
        losses = {}

        # Discriminator part - Ground truth Input
        if fp == 1:
            self.label.data.fill_(self.real_label)
            output_1 = self.netD(ground_truth).view(-1)
            if output_1.shape[0] != self.label.shape[0]:
                print('f1')
            else:
                losses['d_real'] = bce_loss(output_1, self.label)

            return losses

        # Discriminator part - Generator Input
        if fp == 2:
            # get results from generator network
            x1, x2 = self.netG(x, mask)

            # replace the missing data with network result
            x2_inpaint = x2 * mask + x * (1. - mask)
            self.label.data.fill_(self.fake_label)

            output = self.netD(x2_inpaint.detach()).view(-1)

            if output.shape[0] != self.label.shape[0]:
                print('f2')
            else:
                losses['d_fake_g'] = bce_loss(output, self.label)

            return losses

        # Generator part
        elif fp == 3:

            # get results from generator network
            x1, x2 = self.netG(x, mask)
            local_patch_gt = local_patch(ground_truth, box)
            # replace missing data with network result
            x1_inpaint = x1 * mask + x * (1. - mask)
            x2_inpaint = x2 * mask + x * (1. - mask)

            local_patch_x1_inpaint = local_patch(x1_inpaint, box)
            local_patch_x2_inpaint = local_patch(x2_inpaint, box)

            # compute BCE-loss for Generator
            self.label.data.fill_(self.real_label)
            output = self.netD(x2_inpaint.detach()).view(-1)
            if output.shape[0] != self.label.shape[0]:
                print('f3')
            else:
                losses['d_real_g'] = bce_loss(output, self.label)

            # compute local and global L1-Losses
            losses['l1_local'] = l1_loss(local_patch_x1_inpaint, local_patch_gt) * \
                self.config['coarse_l1_lambda'] + \
                l1_loss(local_patch_x2_inpaint, local_patch_gt)
            losses['l1_global'] = l1_loss(x1 * (1. - mask), ground_truth * (1. - mask)) * \
                self.config['coarse_l1_lambda'] + \
                l1_loss(x2 * (1. - mask), ground_truth * (1. - mask))

            return losses, x2_inpaint, x1, x2

    def save_model(self, checkpoint_dir, iteration):
        # Save generators, discriminators, and optimizers
        gen_name = os.path.join(checkpoint_dir, 'gen_%08d.pt' % iteration)
        dis_name = os.path.join(checkpoint_dir, 'dis_%08d.pt' % iteration)
        opt_name = os.path.join(checkpoint_dir, 'optimizer.pt')
        torch.save(self.netG.state_dict(), gen_name)
        torch.save({'netD': self.netD.state_dict(),
                    }, dis_name)
        torch.save({'gen': self.optimizer_g.state_dict(),
                    'dis': self.optimizer_d.state_dict()}, opt_name)

    def resume(self, last_model_name_g, last_model_name_d=None, iteration=0, test=False):
        # Load generators
        self.netG.load_state_dict(torch.load(last_model_name_g))

        if not test:
            # Load discriminators
            state_dict = torch.load(last_model_name_d)
            self.netD.load_state_dict(state_dict['netD'])
            # Load optimizers
            state_dict = torch.load('optimizer.pt')
            self.optimizer_d.load_state_dict(state_dict['dis'])
            self.optimizer_g.load_state_dict(state_dict['gen'])

        print("Resume at iteration {}".format(iteration))
        logger.info("Resume at iteration {}".format(iteration))

        return iteration

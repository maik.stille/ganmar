from __future__ import print_function, division
import torch
import numpy as np
import matplotlib.pyplot as plt
from skimage import io, transform
from torch.utils.data import Dataset, DataLoader
import h5py

# Ignore warnings
import warnings
warnings.filterwarnings("ignore")

plt.ion()   # interactive mode


class SinogramDataset(Dataset):
    """Simulated Sinogram Data from the XCAT Phantom """

    def __init__(self, number_images, root_dir, transform=None):

        """
        :param number_images (int): number of images
        :param root_dir (string): Directory with all images
        :param transform (callable, optional): Optional transform to be applied on a sample image

        """
        self.data_labels_hdf5 = h5py.File(root_dir, 'r')
        self.transform = transform
        self.number_images = number_images

    def __len__(self):
        return self.number_images

    def __getitem__(self, item, dim=512):

        if torch.is_tensor(item):
            item = item.tolist()

        image_ar = np.array(self.data_labels_hdf5['Data'][item])
        image_2_ar = np.array(self.data_labels_hdf5['Labels'][item])
        mask_ar = np.array(self.data_labels_hdf5['Mask'][item])

        mask_ar = np.where(mask_ar == 0, mask_ar, 1)
        sample = {'image': image_ar[:, :], 'image_complete': image_2_ar[:, :], 'mask': mask_ar[:, :]}

        if self.transform:
            sample = self.transform(sample)

        return sample


class Rescale(object):
    """Rescale the image in a sample to a given size.

    Args:
        output_size (tuple or int): Desired output size. If tuple, output is
            matched to output_size. If int, smaller of image edges is matched
            to output_size keeping aspect ratio the same.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        self.output_size = output_size

    def __call__(self, sample):
        image, image_complete = sample['image'], sample['image_complete']

        h, w = image.shape[:2]
        if isinstance(self.output_size, int):
            if h > w:
                new_h, new_w = self.output_size * h / w, self.output_size
            else:
                new_h, new_w = self.output_size, self.output_size * w / h
        else:
            new_h, new_w = self.output_size

        new_h, new_w = int(new_h), int(new_w)

        img = transform.resize(image, (new_h, new_w))
        img_complete = transform.resize(image_complete, (new_h, new_w))

        return {'image': img, 'image_complete': img_complete}


class RandomCrop(object):
    """Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        if isinstance(output_size, int):
            self.output_size = (output_size, output_size)
        else:
            assert len(output_size) == 2
            self.output_size = output_size

    def __call__(self, sample):
        image, image_complete = sample['image'], sample['image_complete']

        h, w = image.shape[:2]
        new_h, new_w = self.output_size

        top = np.random.randint(0, h - new_h)
        left = np.random.randint(0, w - new_w)

        image = image[top: top + new_h,
                      left: left + new_w]
        image_complete = image_complete[top: top + new_h,
                      left: left + new_w]

        return {'image': image, 'image_complete': image_complete}



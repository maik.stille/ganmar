import os
import torch
import yaml
import numpy as np
from PIL import Image
import torch.nn.functional as F
import sys
import datetime
import logging


def date_uid():
    """Generate a unique id based on date.

    Returns:
        str: Return id string.

    """
    return str(datetime.datetime.now()).replace('-', '') \
        .replace(' ', '').replace(':', '').replace('.', '')


def get_logger(checkpoint_path=None):
    """
    Get the root logger
    :param checkpoint_path: only specify this when the first time call it
    :return: the root logger
    """
    if checkpoint_path:
        logger = logging.getLogger()
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        stream_hdlr = logging.StreamHandler(sys.stdout)
        log_filename = date_uid()
        file_hdlr = logging.FileHandler(os.path.join(checkpoint_path, log_filename + '.log'))
        stream_hdlr.setFormatter(formatter)
        file_hdlr.setFormatter(formatter)
        logger.addHandler(stream_hdlr)
        logger.addHandler(file_hdlr)
        logger.setLevel(logging.INFO)
    else:
        logger = logging.getLogger()
    return logger


def random_box(config, mask):
    """Generate a random box in the mask area

    Args:
        config: parameter settings for image and mask
        mask: area of the metal trace

    Returns:
        tuple: (top, left, height, width)

    """
    img_height, img_width, _ = config['image_shape']
    h, w = config['mask_shape']
    box_list = []
    mask_ar = mask.detach().numpy()
    for i in range(config['batch_size']):
        slice_mask = mask_ar[i, :, :]
        idx = np.where(slice_mask > 0)
        idx_n = np.random.randint(idx[0].shape[0])
        t = idx[0][idx_n]
        l = idx[1][idx_n]
        box_list.append((t, l, h, w))

    return torch.tensor(box_list, dtype=torch.int64)


def local_patch(x, box_list):
    """
    create a local patch around a mask region

    :param x: input_data
    :param box_list: indices for local patch generation
    :return:
    patches: local image patch for each image in the batch
    """
    assert len(x.size()) == 4
    patches = []
    for i, box in enumerate(box_list):
        t, l, h, w = box
        if t + h < 128 and l + w < 128:
            patches.append(x[i, :, t:t + h, l:l + w])
        elif t + h < 128:
            patches.append(x[i, :,  t:t + h,  l - w:l])
        elif l + w < 128:
            patches.append(x[i, :, t - h:t, l:l + w])
        else:
            patches.append(x[i, :, t - h:t, l - w:l])
    return torch.stack(patches, dim=0)


# get configs
def get_config(config):
    with open(config, 'r') as stream:
        return yaml.load(stream)


# Get model list for resume
def get_model_list(dir, key, iteration=0):
    if os.path.exists(dir) is False:
        return None
    gen_models = [os.path.join(dir, f) for f in os.listdir(dir) if
                  os.path.isfile(os.path.join(dir, f)) and key in f and ".pt" in f]
    if gen_models is None:
        return None
    gen_models.sort()
    if iteration == 0:
        last_model_name = gen_models[-1]
    else:
        for model_name in gen_models:
            if '{:0>8d}'.format(iteration) in model_name:
                return model_name
        raise ValueError('Not found models with this iteration')
    return last_model_name




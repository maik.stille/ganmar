from load_Data_HDF5 import SinogramDataset
import os
import random
import time
import shutil
from argparse import ArgumentParser
import numpy as np
from skimage.metrics import structural_similarity as ssim
from skimage.metrics import mean_squared_error

import torch
import torch.nn as nn
from torch.utils.data import DataLoader
import torch.backends.cudnn as cudnn
import torchvision.utils as vutils
from tensorboardX import SummaryWriter
from trainer_GAN import Trainer_GAN
from help_functions import get_config, random_box, get_logger
from matplotlib import pyplot as plt

parser = ArgumentParser()
parser.add_argument('--config', type=str, default='configs/config.yaml',
                    help="training configuration")
parser.add_argument('--seed', type=int, help='manual seed')


def main():
    args = parser.parse_args()
    config = get_config(args.config)

    # CUDA configuration
    cuda = config['cuda']
    device_ids = config['gpu_ids']
    if cuda:
        os.environ['CUDA_VISIBLE_DEVICES'] = ','.join(str(i) for i in device_ids)
        device_ids = list(range(len(device_ids)))
        config['gpu_ids'] = device_ids
        cudnn.benchmark = True

    # Configure checkpoint path
    checkpoint_path = os.path.join('checkpoints')
    if not os.path.exists(checkpoint_path):
        os.makedirs(checkpoint_path)
    shutil.copy(args.config, os.path.join(checkpoint_path, os.path.basename(args.config)))
    writer = SummaryWriter(logdir=checkpoint_path)
    logger = get_logger(checkpoint_path)  # get logger and configure it at the first call

    logger.info("Arguments: {}".format(args))
    # Set random seed
    if args.seed is None:
        args.seed = random.randint(1, 10000)
    logger.info("Random seed: {}".format(args.seed))
    random.seed(args.seed)
    torch.manual_seed(args.seed)
    if cuda:
        torch.cuda.manual_seed_all(args.seed)

    # Log the configuration
    logger.info("Configuration: {}".format(config))

    try:
        # Load the datasets for training and validation
        train_dataset = SinogramDataset(number_images=183040,
                                        root_dir=config['train_data_path'])
        train_loader = DataLoader(train_dataset, batch_size=config['batch_size'], shuffle=True,
                                  num_workers=config['num_workers'])

        val_dataset = SinogramDataset(number_images=31040,
                                      root_dir=config['val_data_path'])
        val_loader = DataLoader(val_dataset, batch_size=config['batch_size'], shuffle=True,
                                num_workers=config['num_workers'])

        # Define the trainer
        trainer = Trainer_GAN(config)
        logger.info("\n{}".format(trainer.netG))
        logger.info("\n{}".format(trainer.netD))

        if cuda:
            trainer = nn.parallel.DataParallel(trainer, device_ids=device_ids)
            trainer_module = trainer.module
        else:
            trainer_module = trainer

        # Get the resume iteration to restart training
        start_epoch = trainer_module.resume('gen_00000050.pt', 'dis_00000050.pt', 50, test=False) \
            if config['resume'] else 1

        # prepare plots for losses and validation
        time_count = time.time()
        loss_g = np.zeros(config['batch_size'] * len(train_loader) * (config['niter'] + 1) + 1)
        loss_d = np.zeros(config['batch_size'] * len(train_loader) * (config['niter'] + 1) + 1)
        loss_g_l1_local = np.zeros(config['batch_size'] * len(train_loader) * (config['niter'] + 1) + 1)
        loss_g_l1_global = np.zeros(config['batch_size'] * len(train_loader) * (config['niter'] + 1) + 1)
        loss_g_bce = np.zeros(config['batch_size'] * len(train_loader) * (config['niter'] + 1) + 1)
        mse = np.zeros(config['batch_size'] * len(train_loader) * (config['niter'] + 1) + 1)
        ssim_n = np.zeros(config['batch_size'] * len(train_loader) * (config['niter'] + 1) + 1)
        mse_full = np.zeros((config['niter'] + 1))
        ssim_full = np.zeros((config['niter'] + 1))
        ind = 0
        ind_2 = 0

        # iterate over the defined number of epochs and the number of training data
        for epoch in range(start_epoch - 1, config['niter'] + 1):
            for i, ground_truth in enumerate(train_loader, 0):

                # Get the input data for the networks
                boxes = random_box(config, ground_truth['mask'])
                x = ground_truth['image'].unsqueeze(1)
                mask = ground_truth['mask'].unsqueeze(1)
                if cuda:
                    x = x.cuda()
                    mask = mask.cuda()
                    ground_truth = ground_truth['image_complete'].cuda()

                # Update D
                trainer_module.optimizer_d.zero_grad()

                # Get D-Loss
                # D-loss for real images
                losses_1 = trainer(x, boxes, mask, ground_truth.unsqueeze(1), fp=1)
                # Scalars from different devices are gathered into vectors
                for k in losses_1.keys():
                    if not losses_1[k].dim() == 0:
                        losses_1[k] = torch.mean(losses_1[k])

                losses_1['d_real'].backward()

                # D-loss for fake-images from the Generator
                losses_2 = trainer(x, boxes, mask, ground_truth.unsqueeze(1), fp=2)
                losses_2['d_fake_g'].backward()
                trainer_module.optimizer_d.step()

                # complete D-loss
                losses_2['d'] = losses_2['d_fake_g'] + losses_1['d_real']

                # Update G
                losses_3, inpainted_result, x1, x2 = trainer(x, boxes, mask, ground_truth.unsqueeze(1),
                                                             fp=3)
                # G-Loss (L1-loss global and in a local mask region)
                trainer_module.optimizer_g.zero_grad()
                losses_3['g'] = losses_3['l1_local'] * config['l1_local_loss_lambda'] + \
                                losses_3['l1_global'] * config['l1_global_loss_lambda'] + \
                                losses_3['d_real_g'] * config['gan_loss_alpha']
                losses_3['g'].backward()
                trainer_module.optimizer_g.step()

                # Log losses
                log_losses_3 = ['d_real_g', 'l1_local', 'l1_global', 'g']
                log_losses_2 = ['d_fake_g', 'd']
                log_losses_1 = ['d_real']
                if i % config['print_iter'] == 0:
                    time_count = time.time() - time_count
                    speed = config['print_iter'] / time_count
                    speed_msg = 'speed: %.2f batches/s ' % speed
                    time_count = time.time()
                    message = 'Train, Epoch: [%d/%d], Iter [%d/%d]' % (epoch, config['niter'], i, len(train_loader))
                    for k in log_losses_3:
                        v = losses_3.get(k, 0.)
                        writer.add_scalar(k, v, i)
                        message += '%s: %.6f ' % (k, v)
                    for k in log_losses_2:
                        v = losses_2.get(k, 0.)
                        writer.add_scalar(k, v, i)
                        message += '%s: %.6f ' % (k, v)
                    for k in log_losses_1:
                        v = losses_1.get(k, 0.)
                        writer.add_scalar(k, v, i)
                        message += '%s: %.6f ' % (k, v)
                    message += speed_msg
                    logger.info(message)
                loss_g[ind_2] = losses_3['g']
                loss_d[ind_2] = losses_2['d']
                loss_g_l1_local[ind_2] = losses_3['l1_local']
                loss_g_l1_global[ind_2] = losses_3['l1_global']
                loss_g_bce[ind_2] = losses_3['d_real_g']
                ind_2 += 1

                # visualization of output images
                if epoch % (config['viz_iter']) == 0 and i == len(train_loader) - 1:
                    viz_images = torch.stack([x, mask, x2, inpainted_result, ground_truth.unsqueeze(1)], dim=1)
                    viz_images = viz_images.view(-1, *list(x.size())[1:])
                    vutils.save_image(viz_images,
                                      '%s/niter_%03d.png' % (checkpoint_path, epoch),
                                      nrow=3 * 4,
                                      normalize=True)

                # Save the model for the epoch
                if epoch % config['snapshot_save_iter'] == 0 and i == len(train_loader) - 1:
                    trainer_module.save_model(checkpoint_path, epoch)

            # iterate over the validation dataset
            for j, ground_truth in enumerate(val_loader, 0):

                # Prepare the inputs
                boxes = random_box(config, ground_truth['mask'])
                x = ground_truth['image'].unsqueeze(1)
                mask = ground_truth['mask'].unsqueeze(1)
                if cuda:
                    x = x.cuda()
                    mask = mask.cuda()
                    ground_truth = ground_truth['image_complete'].cuda()

                #  get Generator outputs
                losses_3, inpainted_result, x1, x2 = trainer(x, boxes, mask, ground_truth.unsqueeze(1),
                                                             fp=3)
                losses_3['g'] = losses_3['l1_local'] * config['l1_local_loss_lambda'] + \
                                losses_3['l1_global'] * config['l1_global_loss_lambda'] + \
                                losses_3[
                                    'wgan_g'] * config['gan_loss_alpha']

                if j % config['print_iter'] == 0:
                    time_count = time.time() - time_count
                    speed = config['print_iter'] / time_count
                    speed_msg = 'speed: %.2f batches/s ' % speed
                    time_count = time.time()
                    message = 'Val, Epoch: [%d/%d], Iter: [%d/%d]' % (epoch, config['niter'], j, len(val_loader))
                    log_losses_3 = ['d_real_g', 'l1_local', 'l1_global', 'g']
                    for k in log_losses_3:
                        v = losses_3.get(k, 0.)
                        writer.add_scalar(k, v, j)
                        message += '%s: %.6f ' % (k, v)

                    message += speed_msg
                    logger.info(message)

                for b in range(inpainted_result.shape[0]):
                    imgs_ar = inpainted_result.data.cpu().detach().numpy()
                    gt_ar = ground_truth.unsqueeze(1).data.cpu().detach().numpy()
                    slice_ar = imgs_ar[b, 0, :, :]
                    slice_gt_ar = gt_ar[b, 0, :, :]
                    mse[ind] = mean_squared_error(slice_ar, slice_gt_ar)
                    ssim_n[ind] = ssim(slice_ar, slice_gt_ar, data_range=slice_ar.max() - slice_ar.min())
                    ind += 1

                if epoch % config['snapshot_save_iter'] == 0 and j == len(val_loader) - 1:
                    fig, ax = plt.subplots(1, 2)
                    ax[0].plot(mse[:ind])
                    ax[0].set_title('Mean Squared Error')
                    ax[1].plot(ssim_n[:ind], 'tab:red')
                    ax[1].set_title('SSIM')
                    plt.savefig('Val_{}.png'.format(epoch))
                    mse_full[epoch] = mse[:ind].mean()
                    ssim_full[epoch] = ssim_n[:ind].mean()
                    ind = 0

        fig, ax = plt.subplots(2, 2)
        ax[0, 0].plot(loss_d[:ind_2])
        ax[0, 0].set_title('Discriminator loss')
        ax[0, 1].plot(loss_g[:ind_2], 'tab:red')
        ax[0, 1].set_title('Generator Loss')
        ax[1, 0].plot(loss_g_l1_global[:ind_2], 'tab:red')
        ax[1, 0].set_title('L1-Loss')
        ax[1, 1].plot(loss_g_bce[:ind_2], 'tab:red')
        ax[1, 1].set_title('BCE-Loss')
        plt.savefig('Loss_{}.png'.format(epoch))

        fig, ax = plt.subplots(1, 2)
        ax[0].plot(mse_full)
        ax[0].set_title('Mean Squared Error')
        ax[1].plot(ssim_full, 'tab:red')
        ax[1].set_title('SSIM')
        plt.savefig('Val_full_{}.png'.format(epoch))

    except Exception as e:  # for unexpected error logging
        logger.error("{}".format(e))
        raise e


if __name__ == '__main__':
    main()
